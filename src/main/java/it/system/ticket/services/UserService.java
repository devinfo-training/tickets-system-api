package it.system.ticket.services;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.system.ticket.exceptions.EntityNotFoundException;
import it.system.ticket.models.Ticket;
import it.system.ticket.models.User;
import it.system.ticket.respositories.UserRepository;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public User getUser(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("User not found!"));
    }

    public List<Ticket> getUserTickets(Long id) {
        return this.getUser(id).getCreatedTickets();
    }

    public List<Ticket> getUserResolvedTickets(Long id) {
        return this.getUser(id).getResolvedTickets();
    }

    public User addUser(User user) {
        return userRepository.save(user);
    }

    public User patchUser(User user, Long id) {
        User currentUser = this.getUser(id);
        if (user.getEmail() != null)
            currentUser.setEmail(user.getEmail());
        if (user.getName() != null)
            currentUser.setName(user.getName());
        if (user.getPassword() != null)
            currentUser.setPassword(user.getPassword());
        return userRepository.save(currentUser);
    }

    public List<User> getUsers() {
        Iterable<User> iterable = userRepository.findAll();
        return StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }
}