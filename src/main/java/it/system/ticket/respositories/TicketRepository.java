package it.system.ticket.respositories;

import org.springframework.data.repository.CrudRepository;

import it.system.ticket.models.Ticket;

public interface TicketRepository extends CrudRepository<Ticket, Long> {

}