package it.system.ticket.respositories;

import org.springframework.data.repository.CrudRepository;

import it.system.ticket.models.User;

public interface UserRepository extends CrudRepository<User, Long> {
}